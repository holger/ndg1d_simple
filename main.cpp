// dow(u)/dow(t) + dow(f(u))/dow(x) = 0, a*f(u) = sin(x) with a = 1

#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
#include <vector>
#include <array>
#include <algorithm>
#include <functional>

#include "BasisKlassen/trunk/vector.hpp"
#include "BasisKlassen/trunk/array.hpp"

using namespace std;

class Matrix
{
public:
  Matrix(int m, int n) : m_(m), n_(n) {
    data_ = new double[m_*n_];
  }

  Matrix(Matrix const& matrix) : m_(matrix.m_), n_(matrix.n_) {
    data_ = new double[m_*n_];
    *this = matrix;
  }

  ~Matrix() {
    delete [] data_;
  }
  
  double& operator()(int i, int j) {
    return data_[i*n_ + j];
  }

  double& operator[](int i) {
    return data_[i];
  }

  Matrix& operator=(Matrix const& matrix) {
    for(int i=0; i<n_*m_;i++)
      data_[i] = matrix.data_[i];

    return *this;
  }

  std::array<int, 2> sizes() {
    return {m_, n_};
  }

  int size() {
    return m_*n_;
  }

private:
  int m_, n_;
  double * data_;
};

//declare variables global
double L = 2*M_PI, Ncell = 10., dx = L/Ncell ;
double N = 3; // degree of polynomial
double dt = 0.025, t = 0, t_final = 20; // physical time
int nt = 100;
double a = 1;

inline double initFunction(double x) // initial function
{
  return sin(x);
}

inline double dLagrange(int k, int N, double x) // get derivatives of lagrange polynomial
{
  iszero(k < N);
  static std::array<double, 3> xi = {-1.,0.,1.};
  double value = 0;
  for(int term = 0; term < N; term++) {
    if(term == k) continue;
    double valueTerm = 1./(xi[k] - xi[term]);
    
    for(int i = 0; i<N; i++) {
      if(i == k) continue;
      if(i == term) continue;
      valueTerm *= (x - xi[i])/(xi[k] - xi[i]); }
    value += valueTerm;
  }
  return value;
}

inline double dLag(int k, int point) {
  return dLagrange(k, 3, point - 1);
}

double representation(double x, BK::Array<double,3> values) // representation of solution profile inside cells
{
  return 0.5*x*(x-1)*values[0] + (1-x*x)*values[1] + 0.5*x*(x+1)*values[2];
}

// Core of the solution- discretised equations with b.c and fluxes
Matrix RHS(Matrix &cellVec) {
  
  Matrix rhs(Ncell,3);
  Matrix dLcellVec0(Ncell,3);
  Matrix dLcellVec1(Ncell,3);
  Matrix dLcellVec2(Ncell,3);
  
  for(int i=0; i<Ncell; i++) 
    for(int point = 0; point < 3; point++) {
      dLcellVec0(i,point) = a*cellVec(i,point) * dLag(0, point);
      dLcellVec1(i,point) = a*cellVec(i,point) * dLag(1, point);
      dLcellVec2(i,point) = a*cellVec(i,point) * dLag(2, point);
    }
  
  BK::Vector<double> fluxVecL(Ncell);
  BK::Vector<double> fluxVecR(Ncell);
  for(int i=1; i<Ncell-1; i++) {
    fluxVecL[i] = a*cellVec(i-1, 2);
    fluxVecR[i] = a*cellVec(i, 2);
  }
  
  fluxVecR[0] = a*cellVec(0, 2);
  fluxVecL[Ncell-1] = a*cellVec(Ncell-2, 2);
  
  // ----  computation of numerical flux at the BORDER of the domain.
  
  fluxVecR[Ncell-1] = a*cellVec(Ncell-1, 2);
  fluxVecL[0] = fluxVecR[Ncell-1]; //a*cellVec[Ncell-1][2];

  for(int i=0; i<Ncell; i++) {
    rhs(i, 0) = 2./dx*3. * ( dLcellVec0(i, 0)* 1./3. + dLcellVec0(i, 1)* 4./3. + dLcellVec0(i, 2)* 1./3. + fluxVecL[i]);
    rhs(i, 1) = 2./dx*3./4. * ( dLcellVec1(i, 0)* 1./3. + dLcellVec1(i, 1)* 4./3. + dLcellVec1(i, 2)* 1./3.);
    rhs(i, 2) = 2./dx*3. * ( dLcellVec2(i, 0)* 1./3. + dLcellVec2(i, 1)* 4./3. + dLcellVec2(i, 2)* 1./3. - fluxVecR[i]);
  }
  
  return rhs;
}

int main(){
  Matrix cellVec(Ncell,3); // 1 cell/element of vector **1 cell = 3 nodes,end nodes inclusive**
  Matrix xVec(Ncell,3); // vector for x coordinates
 
 for(int i=0; i<Ncell; i++) { //filling the vector of arrays with x coords and init_function values
    xVec(i, 0) = i*dx;
    xVec(i, 1) = (2.*i+1)/2.*dx;
    xVec(i, 2) = (i+1)*dx;
    cellVec(i, 0) = initFunction(i*dx);
    cellVec(i, 1) = initFunction((2.*i+1)/2.*dx);
    cellVec(i, 2) = initFunction((i+1)*dx);
  }
 std::ofstream initout("init.data"); // printing x coords and initial function to a file
 for(int i=0; i<Ncell; i++)
    for(int j=0; j<3; j++) {
     initout << xVec(i, j) << "\t" << cellVec(i, j) << std::endl;
    }
 initout.close();

 for (int step = 0; step < nt; step++) {
   std::string filename = "solution-" + std::to_string(step) + ".data";
   std::ofstream out(filename); // printing solution to a file
   for(int i=0; i<Ncell; i++)
     for(int j=0; j<3; j++) {
       out << xVec(i, j) << "\t" << cellVec(i, j) << std::endl;
     }
   out.close();
   
   filename = "solution_fine-" + std::to_string(step) + ".data";
   out.open(filename);
   int nInCell = 10;
   double dxInCell = dx/(nInCell-1);
   for(int i=0; i<Ncell; i++)
     for(int j=0; j<nInCell; j++) {
       BK::Array<double,3> values = {cellVec(i,0), cellVec(i,1), cellVec(i,2)};
       out << i*dx+j*dxInCell << "\t" << representation(2*(j*dxInCell/dx)-1, values) << std::endl;
     }
   out.close();
   
   Matrix k1 = RHS( cellVec );
   Matrix temp(cellVec);
   for(int i=0; i<cellVec.size(); i++)
     temp[i] += dt*0.5*k1[i];
     
   Matrix k2 = RHS( temp );
   temp = cellVec;
   for(int i=0; i<cellVec.size(); i++)
     temp[i] += dt*0.5*k2[i];
   
   Matrix k3 = RHS( temp );
   temp = cellVec;
   for(int i=0; i<cellVec.size(); i++)
     temp[i] += dt*k3[i];
   
   Matrix k4 = RHS( temp );
   
   for(int i=0; i<Ncell; i++)
     for(int j=0; j<3; j++) {
       cellVec(i, j) = cellVec(i, j)+ ((k1(i, j) + 2.0 * k2(i, j) + 2.0 * k3(i, j) + k4(i, j)) * dt / 6.0) ; }

 }

 int retval = system("diff solution_fine-99.data solution_fine-99_ref.data");
 std::cout << "solution is ok? = " << !retval << std::endl;
 return 0;
}
