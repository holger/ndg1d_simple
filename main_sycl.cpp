// dow(u)/dow(t) + dow(f(u))/dow(x) = 0, a*f(u) = sin(x) with a = 1

#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
#include <vector>
#include <array>
#include <algorithm>
#include <functional>

#include "BasisKlassen/trunk/vector.hpp"
#include "BasisKlassen/trunk/array.hpp"

#include <CL/sycl.hpp>

using namespace std;
using namespace cl::sycl;
using namespace std;


class Matrix
{
public:
  Matrix(int m, int n, queue Q) : m_(m), n_(n), Q_(Q) {
    data_ = malloc_shared<double>(m_*n_, Q);
  }

  Matrix(Matrix const& matrix) : m_(matrix.m_), n_(matrix.n_) {
    data_ = malloc_shared<double>(m_*n_, Q_);
    *this = matrix;
  }

  ~Matrix() {
    free(data_, Q_);
  }

  void resize(int m, int n, queue Q) {
    m_ = m;
    n_ = n;
    data_ = malloc_shared<double>(m_*n_, Q); 
  }
  
  double& operator()(int i, int j) {
    return data_[i*n_ + j];
  }

  double& operator[](int i) {
    return data_[i];
  }

  Matrix& operator=(Matrix const& matrix) {
    for(int i=0; i<n_*m_;i++)
      data_[i] = matrix.data_[i];

    return *this;
  }

  std::array<int, 2> sizes() {
    return {m_, n_};
  }

  int size() {
    return m_*n_;
  }

  double * data() {
    return data_;
  }

private:
  int m_, n_;
  double * data_;
  queue Q_;
};

Matrix * getMatrix_shared(int m, int n, queue Q) {
  Matrix * matrix = malloc_shared<Matrix>(1, Q);
  matrix->resize(m, n, Q);
  return matrix;
}

//declare variables global
double L = 2*M_PI, Ncell = 10., dx = L/Ncell ;
double N = 3; // degree of polynomial
double dt = 0.025, t = 0, t_final = 20; // physical time
int nt = 100;
double a = 1;

inline double initFunction(double x) // initial function
{
  return sin(x);
}

inline double dLagrange(int k, int N, double x) // get derivatives of lagrange polynomial
{
  iszero(k < N);
  static std::array<double, 3> xi = {-1.,0.,1.};
  double value = 0;
  for(int term = 0; term < N; term++) {
    if(term == k) continue;
    double valueTerm = 1./(xi[k] - xi[term]);
    
    for(int i = 0; i<N; i++) {
      if(i == k) continue;
      if(i == term) continue;
      valueTerm *= (x - xi[i])/(xi[k] - xi[i]); }
    value += valueTerm;
  }
  return value;
}

inline double dLag(int k, int point) {
  return dLagrange(k, 3, point - 1);
}

double representation(double x, BK::Array<double,3> values) // representation of solution profile inside cells
{
  return 0.5*x*(x-1)*values[0] + (1-x*x)*values[1] + 0.5*x*(x+1)*values[2];
}

// Core of the solution- discretised equations with b.c and fluxes
Matrix RHS(Matrix &cellVec, queue& Q) {
  
  Matrix rhs(Ncell,3, Q);
  Matrix dLcellVec0(Ncell,3, Q);
  Matrix dLcellVec1(Ncell,3, Q);
  Matrix dLcellVec2(Ncell,3, Q);
  
  for(int i=0; i<Ncell; i++) 
    for(int point = 0; point < 3; point++) {
      dLcellVec0(i,point) = a*cellVec(i,point) * dLag(0, point);
      dLcellVec1(i,point) = a*cellVec(i,point) * dLag(1, point);
      dLcellVec2(i,point) = a*cellVec(i,point) * dLag(2, point);
    }
  
  BK::Vector<double> fluxVecL(Ncell);
  BK::Vector<double> fluxVecR(Ncell);
  for(int i=1; i<Ncell-1; i++) {
    fluxVecL[i] = a*cellVec(i-1, 2);
    fluxVecR[i] = a*cellVec(i, 2);
  }
  
  fluxVecR[0] = a*cellVec(0, 2);
  fluxVecL[Ncell-1] = a*cellVec(Ncell-2, 2);
  
  // ----  computation of numerical flux at the BORDER of the domain.
  
  fluxVecR[Ncell-1] = a*cellVec(Ncell-1, 2);
  fluxVecL[0] = fluxVecR[Ncell-1]; //a*cellVec[Ncell-1][2];

  for(int i=0; i<Ncell; i++) {
    rhs(i, 0) = 2./dx*3. * ( dLcellVec0(i, 0)* 1./3. + dLcellVec0(i, 1)* 4./3. + dLcellVec0(i, 2)* 1./3. + fluxVecL[i]);
    rhs(i, 1) = 2./dx*3./4. * ( dLcellVec1(i, 0)* 1./3. + dLcellVec1(i, 1)* 4./3. + dLcellVec1(i, 2)* 1./3.);
    rhs(i, 2) = 2./dx*3. * ( dLcellVec2(i, 0)* 1./3. + dLcellVec2(i, 1)* 4./3. + dLcellVec2(i, 2)* 1./3. - fluxVecR[i]);
  }
  
  return rhs;
}

int main(){

  //sycl::default_selector device_selector;
  sycl::default_selector device_selector;
  queue Q(device_selector);
  
  auto platforms = sycl::platform::get_platforms();
  
  for (auto &platform : platforms) {
    
    std::cout << "Platform: "
	      << platform.get_info<sycl::info::platform::name>()
	      << std::endl;
    
    auto devices = platform.get_devices();
    for (auto &device : devices ) {
      std::cout << "  Device: "
		<< device.get_info<sycl::info::device::name>()
		<< std::endl;
    }
  }

  std::cout << "using device : " << Q.get_device().get_info<sycl::info::device::name>() << std::endl;
  
  Matrix * cellVecp = getMatrix_shared(Ncell, 3, Q);
  Matrix& cellVec = *cellVecp;

  //  Matrix cellVec(Ncell,3); // 1 cell/element of vector **1 cell = 3 nodes,end nodes inclusive**
  Matrix xVec(Ncell, 3, Q); // vector for x coordinates
 
 for(int i=0; i<Ncell; i++) { //filling the vector of arrays with x coords and init_function values
    xVec(i, 0) = i*dx;
    xVec(i, 1) = (2.*i+1)/2.*dx;
    xVec(i, 2) = (i+1)*dx;
    cellVec(i, 0) = initFunction(i*dx);
    cellVec(i, 1) = initFunction((2.*i+1)/2.*dx);
    cellVec(i, 2) = initFunction((i+1)*dx);
  }
 std::ofstream initout("init.data"); // printing x coords and initial function to a file
 for(int i=0; i<Ncell; i++)
    for(int j=0; j<3; j++) {
     initout << xVec(i, j) << "\t" << cellVec(i, j) << std::endl;
    }
 initout.close();

 for (int step = 0; step < nt; step++) {
   std::string filename = "solution-" + std::to_string(step) + ".data";
   std::ofstream out(filename); // printing solution to a file
   for(int i=0; i<Ncell; i++)
     for(int j=0; j<3; j++) {
       out << xVec(i, j) << "\t" << cellVec(i, j) << std::endl;
     }
   out.close();
   
   filename = "solution_fine-" + std::to_string(step) + ".data";
   out.open(filename);
   int nInCell = 10;
   double dxInCell = dx/(nInCell-1);
   for(int i=0; i<Ncell; i++)
     for(int j=0; j<nInCell; j++) {
       BK::Array<double,3> values = {cellVec(i,0), cellVec(i,1), cellVec(i,2)};
       out << i*dx+j*dxInCell << "\t" << representation(2*(j*dxInCell/dx)-1, values) << std::endl;
     }
   out.close();

   Matrix * k1p = getMatrix_shared(Ncell, 3, Q);
   Matrix * k2p = getMatrix_shared(Ncell, 3, Q);
   Matrix * k3p = getMatrix_shared(Ncell, 3, Q);
   Matrix * k4p = getMatrix_shared(Ncell, 3, Q);
   Matrix& k1 = *k1p;
   Matrix& k2 = *k2p;
   Matrix& k3 = *k3p;
   Matrix& k4 = *k4p;
      
   k1 = RHS( cellVec, Q );
   Matrix temp(cellVec);
   for(int i=0; i<cellVec.size(); i++)
     temp[i] += dt*0.5*k1[i];
     
   k2 = RHS( temp, Q );
   temp = cellVec;
   for(int i=0; i<cellVec.size(); i++)
     temp[i] += dt*0.5*k2[i];
   
   k3 = RHS( temp, Q );
   temp = cellVec;
   for(int i=0; i<cellVec.size(); i++)
     temp[i] += dt*k3[i];

   k4 = RHS( temp, Q );

   double * dtp = malloc_shared<double>(1,Q);
   *dtp = dt;
   
   cl::sycl::range<2> dims{size_t(Ncell), 3};
   
   Q.submit([&](handler& h) {
	      h.parallel_for(dims, [=](cl::sycl::id<2> idx) {
				     int i = idx[0];
				     int j = idx[1];
				     
				     Matrix& kk1 = *k1p;
				     Matrix& kk2 = *k2p;
				     Matrix& kk3 = *k3p;
				     Matrix& kk4 = *k4p;
				     Matrix& c = *cellVecp;
				     
				     c(i, j) = c(i, j) + ((kk1(i, j) + 2.0 * kk2(i, j) + 2.0 * kk3(i, j) + kk4(i, j)) * (*dtp) / 6.0) ; 
				   });
	    });
   Q.wait();
 }
 
 int retval = system("diff solution_fine-99.data solution_fine-99_ref.data");
 std::cout << "solution is ok? = " << !retval << std::endl;
 return 0;
}
