#include <CL/sycl.hpp>


using namespace cl::sycl;

int main()
{
  constexpr int N = 10;   // number of data elements
  
  queue Q;                // execution queue on DEVICE
  
  double* input = malloc_shared<double>(N, Q);  // flexible data allocation 
  double* result = malloc_shared<double>(N, Q); // on CPU & DEVICE
  
  for (int i = 0; i < N; i++)   // Initialisation on CPU
    input[i] = i;

  // computation of result = input + 1 on DEVICE
  Q.submit([&](handler& h) {
	     h.parallel_for(N,
			    // Lambda-Kernel
			    [=](id<1> i) { result[i] = input[i] + 1; } 
			    
			    );
	   });
  Q.wait();

  for (int i = 0; i < N; i++)  // data output on CPU
    std::cout << "i=" << i << ":\t" << input[i] << "\t" << result[i]
	      << std::endl;

  free(result, Q);
  free(input, Q);
}
