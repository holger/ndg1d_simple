#include <vector>
#include <algorithm>
#include <iostream>


int main()
{
  constexpr int N = 10;   // number of data elements
  

  
  std::vector<double> input(N); // data allocation
  std::vector<double> result(N);
  
  for (int i = 0; i < N; i++)   // Initialisation
    input[i] = i;

  // computation of result = input + 1:
  std::transform(input.cbegin(), input.cend(), result.begin(),
		 
		 [=](double in){ return in + 1;} // lambda-function
		 
		 );





  for (int i = 0; i < N; i++)  // data output on CPU
    std::cout << "i=" << i << ":\t" << input[i] << "\t" << result[i]
	      << std::endl;

}
